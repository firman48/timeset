﻿using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RPL_OrdersDesktop_Client.Services {
    public interface IServices<T> {
        ObservableCollection<T> ItemData { get; set; }
        List<T> Items { get; set; }
        Task<bool> Event_ReadAsync(List<T> item);
        Task<bool> Event_ReadAsync();
        Task Event_CreateAsync(T model);
        Task Event_UpdateAsync(T model);
        Task Event_DeleteAsync(T model);
    }
}
