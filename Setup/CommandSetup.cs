﻿using System;
using System.Windows.Input;

namespace RPL_OrdersDesktop_Client.Setup {
    public class CommandSetup : ICommand {
        private readonly Action cmdactionnone;
        private readonly Action<object> cmdaction;

        public CommandSetup(Action cmdactionnone) {
            this.cmdactionnone = cmdactionnone;
        }

        public CommandSetup(Action<object> cmdaction) {
            this.cmdaction = cmdaction;
        }

        public bool CanExecute(object parameter) {
            return true;
        }

        public void Execute(object parameter) {
            if (parameter != null) {
                cmdaction(parameter);
            } else {
                cmdactionnone();
            }
        }

        public event EventHandler CanExecuteChanged {
            add {
                CommandManager.RequerySuggested += value;
            }

            remove {
                CommandManager.RequerySuggested -= value;
            }
        }
    }
}
