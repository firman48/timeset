﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows;
using Newtonsoft.Json.Linq;

using RPL_DataAccess_Library.Data;
using RPL_DataAccess_Library.Repositories;
using RPL_DataAccess_Library;
using RPL_OrdersDesktop_Client.Setup;
using RPL_OrdersDesktop_Client.Models;

namespace RPL_OrdersDesktop_Client.ViewModels {
    internal delegate Task<bool> CallDataOrdersDetailTask(List<OrdersDetailModel> item);

    public class OrdersDetailViewModel : BaseViewModel {
        private readonly Repository<OrdersDetail> _repo = new OrdersDetailRepository();

        private ObservableCollection<OrdersDetailModel> _itemdata;
        private OrdersDetailModel _model;

        internal event PushComponent OnPushComponent;

        public OrdersDetailViewModel() {
            _model = new OrdersDetailModel();
            _itemdata = new ObservableCollection<OrdersDetailModel>();
            //SaveCommand = new CommandSetup(async () => await Event_CreateAsync());
            //UpdateCommand = new CommandSetup(async () => await Event_UpdateAsync());
            //DeleteCommand = new CommandSetup(async () => await Event_DeleteAsync());
            //ResetCommand = new CommandSetup(async () => await Event_ResetAsync());
            Task.Run(async () => await Event_ReadAsync()).Wait();
            Title = "Form Product Editor";
        }

        public OrdersDetailModel Model {
            get {
                return _model;
            }
            set {
                SetProperty(ref _model, value);
            }
        }

        public ObservableCollection<OrdersDetailModel> ItemData {
            get {
                return _itemdata;
            }
            set {
                SetProperty(ref _itemdata, value);
            }
        }

        public ICommand SaveCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand ResetCommand { get; set; }

        private async Task<bool> Event_ReadAsync() {
            var items = new List<OrdersDetailModel>();

            try {
                foreach (var value in JArray.Parse(_repo.DataSerialized)) {
                    var result = JObject.Parse(value.ToString());
                    items.Add(_model.Entity(result));
                }
            } catch (Exception e) {
                Console.WriteLine(e.Message);
                return await Task.FromResult(false);
            } finally {
                ItemData.Clear();
                foreach (var item in items) {
                    ItemData.Add(item);
                }
            }
            return await Task.FromResult(true);
        }
    }
}
