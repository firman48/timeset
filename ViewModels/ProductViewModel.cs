﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;

using RPL_OrdersDesktop_Client.Setup;
using RPL_OrdersDesktop_Client.Models;

namespace RPL_OrdersDesktop_Client.ViewModels {
    public class ProductViewModel : BaseViewModel {
        private ObservableCollection<ProductModel> itemdata;
        private ProductModel model;

        public ProductViewModel() {
            model = new ProductModel();
            itemdata = ServProduct.ItemData;

            SaveCommand = new CommandSetup(async () => {
                await ServProduct.Event_CreateAsync(Model);
                OnPushComponent?.Invoke(this, new PushComponentEvent());
            });

            UpdateCommand = new CommandSetup(async () => {
                await ServProduct.Event_UpdateAsync(Model);
                OnPushComponent?.Invoke(this, new PushComponentEvent());
            });

            DeleteCommand = new CommandSetup(async () => {
                await ServProduct.Event_DeleteAsync(Model);
                OnPushComponent?.Invoke(this, new PushComponentEvent());
            });

            ResetCommand = new CommandSetup(async () => {
                await ServProduct.Event_ReadAsync();
                OnPushComponent?.Invoke(this, new PushComponentEvent());
            });

            var task = new CallDataProductTask(ServProduct.Event_ReadAsync);
            var asyncprogres = task.BeginInvoke(ServProduct.Items, new AsyncCallback(LoadDataCallback), null);
            task.EndInvoke(asyncprogres);

            Title = "Form Product Editor";
        }

        public ObservableCollection<ProductModel> ItemData {
            get {
                return itemdata;
            }
            set {
                SetProperty(ref itemdata, value);
            }
        }

        public ProductModel Model {
            get {
                return model;
            }
            set {
                SetProperty(ref model, value);
            }
        }

        public ICommand SaveCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand ResetCommand { get; set; }

        private async void LoadDataCallback(IAsyncResult result) {
            await Task.Run(() => {
                ItemData.Clear();
                foreach (var item in ServProduct.Items) {
                    ItemData.Add(item);
                }
            });
        }

        public event PushComponent OnPushComponent;
    }
}
