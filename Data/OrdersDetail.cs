﻿namespace RPL_DataAccess_Library.Data {
    public class OrdersDetail : Entity {
        public override string Id { get; set; }
        public Orders Orders { get; set; } = new Orders();
        public string Qty { get; set; }
        public string Amount { get; set; }
    }
}
