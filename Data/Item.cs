﻿namespace RPL_DataAccess_Library.Data {
    public class Item : Entity {
        public override string Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Price { get; set; }
    }
}
