﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;

using RPL_OrdersDesktop_Client.Models;

namespace RPL_OrdersDesktop_Client {
    public delegate void PushComponent(object sender, PushComponentEvent pushevent);
    public delegate Task<bool> CallDataOrdersTask(List<OrdersModel> item);
    public delegate Task<bool> CallDataProductTask(List<ProductModel> item);

    public partial class App : Application {
        private void Application_Startup(object sender, StartupEventArgs e) {
            new Views.Main.MainView().Show();
        }
    }

    public class PushComponentEvent : EventArgs {
        public PushComponentEvent() {

        }
    }
}
