﻿using RPL_DataAccess_Library.Data;

namespace RPL_OrdersDesktop_Client.Models {
    public class OrdersModel : Orders {
        public OrdersModel Entity(dynamic result) {
            var entity = new OrdersModel() {
                Id = result["Id"].ToString() as string,
                CurrDate = result["CurrDate"].ToString() as string,
                Customer = result["Customer"].ToString() as string,
                Total = result["Total"].ToString() as string,
            };
            return entity;
        }
    }
}
