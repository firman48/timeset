﻿using RPL_DataAccess_Library.Data;

namespace RPL_OrdersDesktop_Client.Models {
    public class ProductModel : Product {
        public ProductModel Entity(dynamic result) {
            var entity = new ProductModel() {
                Id = result["Id"].ToString() as string,
                Name = result["Name"].ToString() as string,
                Price = result["Price"].ToString() as string,
                Guarantee = result["Guarantee"].ToString() as string,
            };
            return entity;
        }
    }
}
