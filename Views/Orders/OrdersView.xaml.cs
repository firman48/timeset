﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

using RPL_OrdersDesktop_Client.ViewModels;
using RPL_OrdersDesktop_Client.Models;

namespace RPL_OrdersDesktop_Client.Views.Orders {
    public partial class OrdersView : UserControl {
        private readonly OrdersViewModel _vm;

        public OrdersView() {
            InitializeComponent();
            _vm = new OrdersViewModel();
            //_vm.OnPushComponent += ViewModel_PushComponent;
            DataContext = _vm;
        }

        private void Txt_KeyDown(object sender, KeyEventArgs e) {
            if (sender.Equals(TxtFind)) {
                if (e.Key == Key.Enter) {
                    BtnFindMin.Focus();
                } else if (e.Key == Key.Escape) {
                    TxtFind.Visibility = BtnFindMin.Visibility = Visibility.Hidden;
                }
            } else if (sender.Equals(TxtDate1)) {
                if (e.Key == Key.Enter) {
                    if (TxtDate1.Text != "__-__-____") {
                        try {
                            TxtDate1.Text = Convert.ToDateTime(TxtDate1.Text.Replace("_", "")).ToString("ddMMyyyy");
                        } catch {
                            TxtDate1.Text = "";
                        }
                    } else {
                        TxtDate1.Text = Convert.ToDateTime(DateTime.Now).ToString("ddMMyyyy"); ;
                    }
                    TxtDate2.Focus();
                } else if (e.Key == Key.Escape) {
                    BtnClose.Focus();
                }
            } else if (sender.Equals(TxtDate2)) {
                if (e.Key == Key.Enter) {
                    if (TxtDate2.Text != "__-__-____") {
                        try {
                            TxtDate2.Text = Convert.ToDateTime(TxtDate2.Text.Replace("_", "")).ToString("ddMMyyyy");
                        } catch {
                            TxtDate2.Text = "";
                        }
                    } else {
                        TxtDate2.Text = Convert.ToDateTime(DateTime.Now).ToString("ddMMyyyy"); ;
                    }
                    BtnApply.Focus();
                } else if (e.Key == Key.Escape) {
                    TxtDate1.Focus();
                }
            }
        }

        private void Txt_GotFocus(object sender, RoutedEventArgs e) {
            var bcolor = new BrushConverter();
            if (sender.Equals(TxtDate1)) {
                TxtDate1.Background = (Brush)bcolor.ConvertFrom("#FFFFF9A9");
            } else if (sender.Equals(TxtDate2)) {
                TxtDate2.Background = (Brush)bcolor.ConvertFrom("#FFFFF9A9");
            } else if (sender.Equals(TxtFind)) {
                TxtFind.Background = (Brush)bcolor.ConvertFrom("#FFFFF9A9");
            }
        }

        private void Txt_LostFocus(object sender, RoutedEventArgs e) {
            var bcolor = new BrushConverter();
            if (sender.Equals(TxtDate1)) {
                TxtDate1.Background = (Brush)bcolor.ConvertFrom("#FFFFFFFF");
            } else if (sender.Equals(TxtDate2)) {
                TxtDate2.Background = (Brush)bcolor.ConvertFrom("#FFFFFFFF");
            } else if (sender.Equals(TxtFind)) {
                TxtFind.Background = (Brush)bcolor.ConvertFrom("#FFFFFFFF");
            }
        }

        private void TblDataGrid_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e) {

        }
    }
}
